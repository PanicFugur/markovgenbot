import logging
import securestuff

import telegram
from telegram.ext import CommandHandler, Updater

import command_handlers
from filter import load_and_filter
from securestuff import REQUEST_KWARGS, kolyan_token, test_token


class MarkovGenBot():
    def __init__(self, use_proxy, use_test_token):
        token = test_token if use_test_token else kolyan_token
        self.Bot = Updater(
            token=token, request_kwargs=REQUEST_KWARGS) if use_proxy else Updater(token=token)
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                            level=logging.INFO)
        # Space for adding handlers

    def start_listening(self):
        self.Bot.start_polling()
        logging.log(logging.INFO, 'Starting polling')
        self.Bot.idle()

    def add_handlers(self):
        dispatcher = self.Bot.dispatcher
        dispatcher.add_handler(CommandHandler('vbros', command_handlers.vbros))


if __name__ == "__main__":
    bot = MarkovGenBot(securestuff.use_proxy, securestuff.production_token)
    bot.add_handlers()
    bot.start_listening()
