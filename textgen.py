from os.path import join
from random import choice
import json
import sys
import random


def openfile(filename):
    try:
        words = []
        with open(filename, 'r', encoding='utf8') as f:
            for line in f:
                words.extend(line.split())
        return words
    except FileNotFoundError:
        print("Couldn't find source file")


def makerule(data, context):
    rule = {}
    words = data
    index = context
    for word in words[index:]:
        key = ' '.join(words[index-context:index])
        if key in rule:
            rule[key].append(word)
        else:
            rule[key] = [word]
        index += 1

    return rule


def get_start_word(filename):
    try:
        first = []
        with open(filename, 'r', encoding='utf8') as f:
            for line in f:
                cur_line = line.split(' ', 1)
                word = cur_line[0]
                first.append(word)
        return first
    except FileNotFoundError:
        print("Couldn't find source file")


def makestring(rule, length):
    # oldwords = choice(list(rule.keys())).split(' ')  # random starting words
    oldwords = []
    st = get_start_word(join('misc', 'output.txt'))
    first = choice(st)
    oldwords.append(first)
    string = ' '.join(oldwords) + ' '
    for i in range(length):
        try:
            key = ' '.join(oldwords)
            newword = choice(rule[key])
            string += newword + ' '

            for word in range(len(oldwords)):
                oldwords[word] = oldwords[(word + 1) % len(oldwords)]
            oldwords[-1] = newword

        except KeyError:
            return string
    return string


def getGenPhrase():
    data = openfile(join('misc', 'output.txt'))
    rule = makerule(data, 1)
    string = makestring(rule, random.randint(5, 15))
    string = string.capitalize()
    return string


if __name__ == '__main__':
    data = openfile(join('misc', 'output.txt'))
    rule = makerule(data, 1)
    string = makestring(rule, random.randint(5, 15))
    print(string.capitalize())
