import sys
from os import remove



result = []


def load_and_filter(file='dialog.txt', outfile='output.txt'):
    with open(file, 'r') as f:
        for line in f:
            if 'Николай Небогатов' in line and len(line.split()) > 3 and ('https://' not in line):
                formline = line.replace('Николай Небогатов :', '')
                formline = formline.lstrip()
                result.append(formline)
    with open(outfile, 'w') as f:
        for line in result:
            f.write(line)


if __name__ == "__main__":
    load_and_filter(sys.argv[1],sys.argv[2])

            
